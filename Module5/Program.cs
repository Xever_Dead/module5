﻿using System;

namespace Module5
{
    class Program
    {
        const int fieldSize = 10;
        const int startHealth = 10;
        const int maxDamage = 10;
        const string playerIcon = "|P";
        const string targetIcon = "|#";
        const string trapIcon = "|*";
        const string fieldCellIcon = "|_";
        const string endRowIcon = "|";
        const string fieldUpperIcon = " _";
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the game. Press any key to start, or esc to quit.");
            var pressedKey = Console.ReadKey().Key;
            Console.Clear();
            if (pressedKey == ConsoleKey.Escape) 
            {
                Console.WriteLine("SSee you next time");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Welcome to the game. Your target is to reach the princess on the other side of the field. Press arrows on your keyboard to move. Princess icon is #, your icon is P. But be careful, there are many traps on your way. Good luck.");
                Console.ReadKey();
                Console.Clear();
                var field = GenerateField();
                DrawField(field, startHealth, "Your journey just begun");
                MakeMove(field, startHealth, 0, 0);
            }
        }

        static string[,] GenerateField()
        {
            var field = new string[fieldSize, fieldSize];
            var randomizer = new Random();

            field[0, 0] = playerIcon;
            field[fieldSize - 1, fieldSize - 1] = targetIcon;

            var trapsPlaced = 0;
            while (trapsPlaced < fieldSize)
            {
                var row = randomizer.Next(0, fieldSize);
                var column = randomizer.Next(0, fieldSize);

                if (field[row, column] == null)
                {
                    field[row, column] = trapIcon;
                    trapsPlaced++;
                }
            }

            return field;
        }

        static void DrawField(string[,] field, int playerHealth, string message)
        {
            Console.Clear();

            Console.WriteLine($"Your health is {playerHealth}");

            for (var column = 0; column < fieldSize; column++)
            {
                Console.Write(fieldUpperIcon);
            }

            Console.WriteLine();

            for (var row = 0; row < fieldSize; row++)
            {
                for (var column = 0; column < fieldSize; column++)
                {
                    if ((field[row, column] == trapIcon) || (field[row, column] == null)) 
                    {
                        Console.Write(fieldCellIcon);
                    }
                    else
                    {
                        Console.Write(field[row, column]);
                    }
                }
                Console.WriteLine(endRowIcon);
            }

            Console.WriteLine(message);
        }

        static void MakeMove(string[,] field, int playerHealth, int playerRow, int playerColumn)
        {
            var pressedKey = Console.ReadKey().Key;
            var message = "You moved ";
            var isTargetReached = false;

            switch(pressedKey)
            {
                case ConsoleKey.UpArrow:
                    if (playerRow == 0)
                    {
                        message = "Can't move there";
                    }
                    else
                    {
                        CheckCell(field, playerRow - 1, playerColumn, out int receivedDamage, ref isTargetReached);

                        field[playerRow, playerColumn] = null;
                        field[--playerRow, playerColumn] = playerIcon;

                        if (receivedDamage > 0)
                        {
                            message = $"You received {receivedDamage} damage.";
                            playerHealth -= receivedDamage;
                        }
                        else
                        {
                            message = string.Concat(message, "up.");
                        }
                    }
                    break;

                case ConsoleKey.DownArrow:
                    if (playerRow == fieldSize - 1)
                    {
                        message = "Can't move there";
                    }
                    else
                    {
                        CheckCell(field, playerRow + 1, playerColumn, out int receivedDamage, ref isTargetReached);

                        field[playerRow, playerColumn] = null;
                        field[++playerRow, playerColumn] = playerIcon;

                        if (receivedDamage > 0)
                        {
                            message = $"You received {receivedDamage} damage.";
                            playerHealth -= receivedDamage;
                        }
                        else
                        {
                            message = string.Concat(message, "down.");
                        }
                    }
                    break;

                case ConsoleKey.LeftArrow:
                    if (playerColumn == 0)
                    {
                        message = "Can't move there";
                    }
                    else
                    {
                        CheckCell(field, playerRow, playerColumn - 1, out int receivedDamage, ref isTargetReached);

                        field[playerRow, playerColumn] = null;
                        field[playerRow, --playerColumn] = playerIcon;

                        if (receivedDamage > 0)
                        {
                            message = $"You received {receivedDamage} damage.";
                            playerHealth -= receivedDamage;
                        }
                        else
                        {
                            message = string.Concat(message, "left.");
                        }
                    }
                    break;

                case ConsoleKey.RightArrow:
                    if (playerColumn == fieldSize - 1) 
                    {
                        message = "Can't move there";
                    }
                    else
                    {
                        CheckCell(field, playerRow, playerColumn + 1, out int receivedDamage, ref isTargetReached);

                        field[playerRow, playerColumn] = null;
                        field[playerRow, ++playerColumn] = playerIcon;

                        if (receivedDamage > 0)
                        {
                            message = $"You received {receivedDamage} damage.";
                            playerHealth -= receivedDamage;
                        }
                        else
                        {
                            message = string.Concat(message, "right.");
                        }
                    }
                    break;
            }

            Console.Clear();

            if (playerHealth <= 0)
            {
                Console.WriteLine("You died on the way to princess.");
                Console.ReadKey();
                Console.Clear();
                Main(null);
            }
            else if (isTargetReached) 
            {
                Console.WriteLine("You reached princess and beat the game.");
                Console.ReadKey();
                Console.Clear();
                Main(null);
            }
            else
            {
                DrawField(field, playerHealth, message);
                MakeMove(field, playerHealth, playerRow, playerColumn);
            }
        }

        static void CheckCell(string[,] field, int row, int column, out int receivedDamage, ref bool isTargetReached)
        {
            receivedDamage = 0;

            if (field[row, column] == trapIcon)
            {
                var randomizer = new Random();

                receivedDamage = randomizer.Next(1, maxDamage + 1);
            }
            else if (field[row, column] == targetIcon)
            {
                isTargetReached = true;
            }
        }
    }
}
